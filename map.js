      var map = new ol.Map({
        layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM()
          }),
          vectorLayer
        ],
        target: 'map',
        view: new ol.View({
          center: ol.proj.fromLonLat([16, 48]),
          zoom: 4
        })
      });
