var vectorSource = new ol.source.Vector({
    features: (new ol.format.GeoJSON({
            dataProjection: 'EPSG:4326',
            featureProjection: 'EPSG:3857'
        })).readFeatures(radars_geojson)
});

function styleFunction(feature, resolution) {
    // Parameterize the fill/stroke colors and alphas
    x = 200;
    c1 = [0, x, 0];
    c2 = [x, 0, 0];
    c3 = [0, 0, x];
    c_other = [x, x, 0];
    af = 0.2;
    as = 0.4;

    properties = feature.getProperties();
    switch (properties["wlan5ghzchannel"]) {
      case 120: color = c1; break;
      case 124: color = c2; break;
      case 128: color = c3; break;
      default: color = c_other;
    }
    return new ol.style.Style({
        stroke: new ol.style.Stroke({color: color.concat(as), width: 7}),
        fill: new ol.style.Fill({color:color.concat(af)})
    });
}

var vectorLayer = new ol.layer.Vector({
    source: vectorSource,
    style: styleFunction
});
