// List the WLAN channels and extra info for the point we are hovering over
// Based on example code: https://openlayers.org/en/latest/examples/select-hover-features.html
// and https://github.com/openlayers/openlayers/issues/6615
// and https://stackoverflow.com/questions/26967638/convert-point-to-lat-lon#26968701

// The info spans sit right below the map 
var coordinfo = document.getElementById('coordinfo');
var channelinfo = document.getElementById("channelinfo");

// The marker. https://stackoverflow.com/questions/55789146/how-to-add-marker-to-open-layer-map

var markerLayer = new ol.layer.Vector({
  source: new ol.source.Vector(),
  style: new ol.style.Style({
    image: new ol.style.Icon({
      anchor: [0.5, 1],
      src: 'marker.png'
    })
  })
});
map.addLayer(markerLayer);


// This tracks whether mouseover causes the infobox to update (--> true), or
// the map was clicked so infos should be displayed statically (--> false).
// Clicking/tapping again toggles this flag.
liveInfoUpdates = true;

// Toggle the live info update field and (de-)register the appropriate
// map event handlers for hovering and clicking.
function interaction(theEvent) {
  if (liveInfoUpdates == false) {
    // We don't update the info live. Only react to clicks / taps.
    if (theEvent.type == "singleclick") {
      // Toggle the update mode, perform update
      liveInfoUpdates = true;
      map.on("pointermove", interaction);
      updateInfoFrom(theEvent);
      markerLayer.getSource().clear();
    }
  } else {
    // We update live. React to pointermoves, but no clicks / taps.
    if (theEvent.type == "singleclick") {
      liveInfoUpdates = false;
      map.on("pointermove", null);
      var marker = new ol.Feature(new ol.geom.Point(theEvent.coordinate));
      markerLayer.getSource().addFeature(marker);
    }
    updateInfoFrom(theEvent);
  }
}

function updateInfoFrom(theEvent) {
    wlanchannels = [];
    map.forEachFeatureAtPixel(theEvent.pixel, function(feature) {
        channel = feature.get("wlan5ghzchannel");
        // Only add this channel if it's not already in the list
        if (wlanchannels.indexOf(channel) == -1 && channel != null) {
            wlanchannels.push(channel);
        };
    });
    wlanchannels.sort();
    channelinfo.innerHTML = "<strong>Expect weather RADARs on these 5 GHz WLAN channels:</strong> " + wlanchannels.join(", ");
      var coords = ol.proj.transform(theEvent.coordinate, 'EPSG:3857', 'EPSG:4326');
    var coordstring = document.getElementById('coordinfo');
    if (liveInfoUpdates) {
        what = "Current";
    } else {
        what = "Marker";
    }
    coordstring.innerHTML = "<strong>" + what + " coordinates:</strong> " + ol.coordinate.createStringXY(4)(coords);

};

map.on("singleclick", interaction);
if (liveInfoUpdates) { map.on("pointermove", interaction);}

