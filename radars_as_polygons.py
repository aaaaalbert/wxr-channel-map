"""
Convert from EUMETNET's OPERA RADARs database JSON file to GeoJSON,
and create a Polygon around each site as a geographic feature.

Usage:

python opera_converter.py YOUR_DB_FILE > DESIRED_OUTPUT_FILE

For each OPERA database entry, create a GeoJSON `Feature` containing
a `Polygon` centered at the RADAR site's `latitude` and `longitude`,
with radius `range`.
Furthermore, use the OPERA `odimcode` as the GeoJSON `id` of the RADAR.

All OPERA fields (including the ones above) are mapped in GeoJSON to
identically named fields under the `properties` key.
For RADARs in the 5 GHz aka "C" band, add a property `wlan5ghzchannel`
with the (best-fit) 20 MHz channel number for 5 GHz WLAN.
"""

import geojson
import json
import math
import sys


# Polygon vertex count for RADAR range indicators
NUMBER_OF_SIDES = 48


def make_polygon(radius, number_of_sides):
    """Return a list of 2D points, sorted in counter-clockwise order,
    that make up a regular polygon of `number_of_sides` sides and radius
    `radius`, centered at [0, 0]. The first point of the polygon is on
    the positive x axis, aka to the right."""
    poly_points = []
    for n in range(number_of_sides):
        angle = n * 2 * math.pi / number_of_sides
        poly_points.append([radius * math.cos(angle),
            radius * math.sin(angle)])
    return poly_points


def add_meter_vector_to_lon_lat_origin(origin, vector):
    """Add a vector (x and y, measured in meters) to the origin (given as
    lon and lat coordinates), and return the lon, lat coordinates of the
    resulting point as a list.
    Caution: This function assumes a spherical Earth. The further away
        from the equator and the larger the vector components, the
        more off the result is!!! Rule of thumb: 1 degree lat or lon at
        the equator is about 111000 meters."
    Note: This function performs no sanity checks on its arguments.."""
    lon, lat = origin
    up, right = vector
    earth_radius = 6378137 # meters, at the equator

    # y shift is easy, degress latitude represent constant lengths
    delta_lat = 90 * up / (math.pi * earth_radius / 2)

    # x shift is semi-difficult: degrees longitude represent different
    # lengths in meters, depending on the latitude we're on.
    circumference_at_lon = 2 * math.pi * earth_radius * math.cos(math.radians(lat))
    delta_lon = 360 * right / circumference_at_lon

    return [lon + delta_lon, lat + delta_lat]




if len(sys.argv) != 2:
    sys.exit("Error: Please supply the OPERA database filename as the sole argument!")

db_filename = sys.argv[1]

try:
    opera_db = json.load(open(db_filename))
except Exception as e:
    sys.exit("Could not open database file: " + str(e)) 


gj = []

for radar in opera_db:
    # Make sure that blank ODIM codes don't break GeoJSON
    radar_id = radar["odimcode"].strip() or None

    # XXX May add `antennaheight` (if absolute) as the z coordinate
    try:
        lon = float(radar["longitude"])
        lat = float(radar["latitude"])
        radar_coords = geojson.Point([lon, lat])
        # This is in kilometers in OPERA, our polygon maker uses meters
        radar_range = 1000 * float(radar["maxrange"])
    except ValueError:
        # This RADAR has strange coordinates. Skip it.
        # TODO: Notify the user of the error, or handle it more gracefully
        continue

    try:
        freq = float(radar["frequency"]) * 1000
        if (5150 <= freq <= 5350) or (5470 <= freq <= 5730):
            # These are the indoor and outdoor 5 GHz WLAN ranges
            channel_number = int(round((freq - 5000) / 5 / 4)) * 4
            radar["wlan5ghzchannel"] = channel_number
    except ValueError:
        # We couldn't make sense of the frequency.
        pass

    # Make polygon from RADAR site origin and range
    radar_polygon_meter_vectors = make_polygon(radar_range, NUMBER_OF_SIDES)

    # Convert RADAR site origin and polygon vectors to list of lat/lon points
    radar_polygon = []
    for vector in radar_polygon_meter_vectors:
        radar_polygon.append(add_meter_vector_to_lon_lat_origin([lon, lat], vector))

    # Note: radar_polygon is just the exterior ring (in GeoJSON speak) and
    # must be the first (even if only) element of a *list* of linear rings.
    gj.append(geojson.Feature(id=radar_id,
        geometry=geojson.Polygon(coordinates=[radar_polygon]), properties=radar))


# Output the results of the conversion as a FeatureCollection,
# let the user pipe to file
print(geojson.dumps(geojson.FeatureCollection(features=gj)))

